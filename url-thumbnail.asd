(defsystem url-thumbnail
  :author "Matt Novenstern <fisxoj@gmail.com>"
  :depends-on ("dexador"
               "lquery")
  :pathname "src/"
  :components ((:file "url-thumbnail")))
