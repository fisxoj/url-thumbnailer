(defpackage url-thumbnail
  (:use :cl))

(in-package :url-thumbnail)

(defparameter +connect-timeout+ 10
  "Connection timeout in seconds.")


(defparameter +read-timeout+ 10
  "Read timeout in seconds.")


(defun thumbnail-url (url)
  (let* ((body (fetch-page url))
         (document (plump:parse body)))))


(defun og-image (document)
  (lquery:$ document
    ()))


;; https://github.com/fukamachi/dexador/issues/91
(defun fetch-page (url)
  (let ((cl+ssl:*default-unwrap-stream-p* nil))
    (dex:get url
             :use-connection-pool t
             :read-timeout +read-timeout+
             :connect-timeout +connect-timeout+)))
